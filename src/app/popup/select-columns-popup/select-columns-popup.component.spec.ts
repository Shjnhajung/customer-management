import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectColumnsPopupComponent } from './select-columns-popup.component';

describe('SelectColumnsPopupComponent', () => {
  let component: SelectColumnsPopupComponent;
  let fixture: ComponentFixture<SelectColumnsPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectColumnsPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectColumnsPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

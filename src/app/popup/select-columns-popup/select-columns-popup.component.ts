import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-select-columns-popup',
  templateUrl: './select-columns-popup.component.html',
  styleUrls: ['./select-columns-popup.component.scss']
})
export class SelectColumnsPopupComponent implements OnInit {
  @Input() columns;

  constructor(public activeModal: NgbActiveModal) {
  }

  getResult() {
    this.activeModal.close(this.columns);
  }

  ngOnInit() {
  }

}

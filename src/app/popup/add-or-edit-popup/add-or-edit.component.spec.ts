import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOrEditComponentComponent } from './add-or-edit-component.component';

describe('AddOrEditComponentComponent', () => {
  let component: AddOrEditComponentComponent;
  let fixture: ComponentFixture<AddOrEditComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddOrEditComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOrEditComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

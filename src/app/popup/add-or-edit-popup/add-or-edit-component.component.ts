import {Component, OnInit, Input} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-or-edit-component',
  templateUrl: './add-or-edit.component.html',
  styleUrls: ['./add-or-edit.component.scss']
})
export class AddOrEditComponentComponent implements OnInit {
  @Input() title;
  @Input() data = {
    email: '',
    first: '',
    last: '',
    company: '',
    country: '',
    type: 'New'
  };

  constructor(public activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

  proceed() {
    this.activeModal.close(this.data);
  }
}

import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AddOrEditComponentComponent } from '../popup/add-or-edit-popup/add-or-edit-component.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmComponent } from '../popup/confirm-popup/confirm.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';
import { SelectColumnsPopupComponent } from '../popup/select-columns-popup/select-columns-popup.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements AfterViewInit, OnDestroy, OnInit {
  @ViewChild(DataTableDirective, { static: false })
  public dtElement: DataTableDirective;
  public dtOptions: DataTables.Settings = {};
  public dtTrigger: Subject<any> = new Subject();
  public persons: any = [];
  public columnsMapping = [
    {
      key: 'ID',
      mappingKey: 'id',
      checked: true,
      disabled: true
    },
    {
      key: 'Email',
      mappingKey: 'email',
      checked: true,
      disabled: true
    },
    {
      key: 'First Name',
      mappingKey: 'first',
      checked: true,
      disabled: false
    },
    {
      key: 'Last Name',
      mappingKey: 'last',
      checked: true,
      disabled: false
    }, {
      key: 'Company Name',
      mappingKey: 'company',
      checked: true,
      disabled: false
    }
  ];

  constructor(private http: HttpClient,
    private modalService: NgbModal) {
  }

  getHeaders(selectedList: Array<any>) {
    const headers = [];
    selectedList.forEach((selectedObj) => {
      if (selectedObj.checked) {
        headers.push(selectedObj.key);
      }
    });
    return headers;
  }

  getChartData() {
    const typesData = [];
    const customerTypes = this.getCustomerTypes();
    customerTypes.forEach((type: string) => {
      const count = _.countBy(this.persons, (person: any) => {
        return person.type === type;
      });
      typesData.push(count.true);
    });
    return typesData;
  }

  getCustomerTypes() {
    const types = [];
    this.persons.forEach((person) => {
      types.push(person.type);
    });
    return _.union(types);
  }

  getRowsByHeaders(selectedList: Array<any>, element) {
    const rows = [];
    selectedList.forEach((selectedObj) => {
      if (selectedObj.checked) {
        rows.push(element[selectedObj.mappingKey]);
      }
    });
    return rows;
  }

  generateExcel() {
    const modalRef = this.modalService.open(SelectColumnsPopupComponent);
    modalRef.result.then((result) => {
      if (result && result.length) {
        console.log(result);

        const title = 'Customer Report';
        const header = this.getHeaders(result);
        const workbook = new Workbook();
        const worksheet = workbook.addWorksheet('Customers');
        worksheet.addRow([title]);
        worksheet.getColumn(1).width = 30;
        worksheet.getColumn(2).width = 30;
        worksheet.getColumn(3).width = 30;
        worksheet.getColumn(4).width = 30;
        worksheet.getColumn(5).width = 30;
        worksheet.addRow([]);
        worksheet.addRow([]);
        worksheet.addRow(header);
        worksheet.addRow([]);
        const rows = [];
        this.persons.forEach(element => {
          const tmp = this.getRowsByHeaders(result, element);
          worksheet.addRow(tmp);
          rows.push(tmp);
        });
        const data = rows;

        workbook.xlsx.writeBuffer().then((data) => {
          const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
          fs.saveAs(blob, 'CustomerReports.xlsx');
        });
      }
    }, (reason) => {
    });
    modalRef.componentInstance.columns = this.columnsMapping;
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  ngOnInit() {
    this.fetchData();
  }

  ngAfterViewInit() {
    this.dtTrigger.next();
  }

  fetchData() {
    if (localStorage.getItem('customers')) {
      this.persons = JSON.parse(localStorage.getItem('customers'));
    }
    else {
      localStorage.setItem('customers', JSON.stringify([
          {
            id: 1,
            email: 'isidro_von@hotmail.com',
            first: 'Torrey',
            last: 'Veum',
            company: 'Hilll, Mayert and Wolf',
            created_at: '2014-12-25T04:06:27.981Z',
            country: 'Switzerland',
            type: 'Potential'
          },
          {
            id: 2,
            email: 'frederique19@gmail.com',
            first: 'Micah',
            last: 'Sanford',
            company: 'Stokes-Reichel',
            created_at: '2014-07-03T16:08:17.044Z',
            country: 'Democratic Peoples Republic of Korea',
            type: 'Potential'
          },
          {
            id: 3,
            email: 'fredy54@gmail.com',
            first: 'Hollis',
            last: 'Swift',
            company: 'Rodriguez, Cartwright and Kuhn',
            created_at: '2014-08-18T06:15:16.731Z',
            country: 'Tunisia',
            type: 'Potential'
          },
          {
            id: 4,
            email: 'braxton29@hotmail.com',
            first: 'Perry',
            last: 'Leffler',
            company: 'Sipes, Feeney and Hansen',
            created_at: '2014-07-10T11:31:40.235Z',
            country: 'Chad',
            type: 'Potential'
          },
          {
            id: 5,
            email: 'turner59@gmail.com',
            first: 'Janelle',
            last: 'Hagenes',
            company: 'Lesch and Daughters',
            created_at: '2014-04-21T15:05:43.229Z',
            country: 'Swaziland',
            type: 'Potential'
          },
          {
            id: 6,
            email: 'halie47@yahoo.com',
            first: 'Charity',
            last: 'Bradtke',
            company: 'Gorczany-Monahan',
            created_at: '2014-09-21T21:59:18.892Z',
            country: 'Lebanon',
            type: 'Potential'
          },
          {
            id: 7,
            email: 'loren_yundt@gmail.com',
            first: 'Dejah',
            last: 'Kshlerin',
            company: 'Williamson-Hickle',
            created_at: '2014-11-11T12:20:53.154Z',
            country: 'Egypt',
            type: 'New'
          },
          {
            id: 8,
            email: 'kenton_macejkovic80@hotmail.com',
            first: 'Ellen',
            last: 'Schaefer',
            company: 'Tillman-Harris',
            created_at: '2014-07-23T02:00:28.649Z',
            country: 'Israel',
            type: 'New'
          },
          {
            id: 9,
            email: 'pascale5@yahoo.com',
            first: 'Sven',
            last: 'Funk',
            company: 'Dare Group',
            created_at: '2014-04-11T12:43:28.977Z',
            country: 'Macao',
            type: 'New'
          },
          {
            id: 10,
            email: 'frank34@yahoo.com',
            first: 'Ryleigh',
            last: 'Cole',
            company: 'Zieme and Daughters',
            created_at: '2014-10-18T05:50:28.626Z',
            country: 'Congo',
            type: 'Impulsive'
          },
          {
            id: 11,
            email: 'harry65@hotmail.com',
            first: 'Cooper',
            last: 'Grimes',
            company: 'Brakus-Rath',
            created_at: '2014-04-29T06:41:20.214Z',
            country: 'Reunion',
            type: 'Impulsive'
          },
          {
            id: 12,
            email: 'kiana.schowalter@gmail.com',
            first: 'Esteban',
            last: 'Homenick',
            company: 'Bode-Botsford',
            created_at: '2014-12-29T18:46:35.269Z',
            country: 'Guadeloupe',
            type: 'Impulsive'
          },
          {
            id: 13,
            email: 'josh_batz60@gmail.com',
            first: 'Lucinda',
            last: 'Waters',
            company: 'Ratke LLC',
            created_at: '2015-03-13T12:15:50.844Z',
            country: 'Lebanon',
            type: 'Impulsive'
          },
          {
            id: 14,
            email: 'zula36@hotmail.com',
            first: 'Jarvis',
            last: 'Grimes',
            company: 'Durgan, Sporer and Bogan',
            created_at: '2014-04-23T23:56:11.268Z',
            country: 'Ghana',
            type: 'Impulsive'
          },
          {
            id: 15,
            email: 'romaine21@gmail.com',
            first: 'Jordon',
            last: 'Turcotte',
            company: 'Green-Haag',
            created_at: '2014-07-13T00:07:36.299Z',
            country: 'Serbia',
            type: 'Impulsive'
          },
          {
            id: 16,
            email: 'abdul3@hotmail.com',
            first: 'Marques',
            last: 'Bins',
            company: 'Hoeger, Frami and Kihn',
            created_at: '2014-04-10T14:07:26.141Z',
            country: 'Sudan',
            type: 'Discount'
          },
          {
            id: 17,
            email: 'van39@hotmail.com',
            first: 'Edgar',
            last: 'Jaskolski',
            company: 'Waelchi-Schuppe',
            created_at: '2014-11-18T22:42:23.788Z',
            country: 'Wallis and Futuna',
            type: 'Discount'
          },
          {
            id: 18,
            email: 'emie_corkery82@yahoo.com',
            first: 'Adell',
            last: 'Rodriguez',
            company: 'Tillman, Bailey and Weimann',
            created_at: '2014-07-19T07:19:38.388Z',
            country: 'Sierra Leone',
            type: 'Discount'
          },
          {
            id: 19,
            email: 'alexis62@hotmail.com',
            first: 'Madonna',
            last: 'Luettgen',
            company: 'Heathcote-Schiller',
            created_at: '2014-08-25T04:29:45.139Z',
            country: 'Namibia',
            type: 'Discount'
          },
          {
            id: 20,
            email: 'lucius_hills53@yahoo.com',
            first: 'Andre',
            last: 'Huel',
            company: 'Stroman Inc',
            created_at: '2014-08-22T22:56:27.222Z',
            country: 'Georgia',
            type: 'Discount'
          },
          {
            id: 21,
            email: 'jeanette.leannon29@hotmail.com',
            first: 'Darrin',
            last: 'Larson',
            company: 'Ernser-Oberbrunner',
            created_at: '2014-09-01T21:22:39.559Z',
            country: 'Lebanon',
            type: 'Discount'
          },
          {
            id: 22,
            email: 'concepcion_kiehn@hotmail.com',
            first: 'Johann',
            last: 'Hintz',
            company: 'Paucek and Sons',
            created_at: '2014-12-29T18:29:33.150Z',
            country: 'Congo',
            type: 'Loyal'
          },
          {
            id: 23,
            email: 'blaze84@yahoo.com',
            first: 'Cruz',
            last: 'Harber',
            company: 'OConnell and Sons',
            created_at: '2014-10-23T09:57:26.941Z',
            country: 'Lesotho',
            type: 'Loyal'
          },
          {
            id: 24,
            email: 'vanessa27@hotmail.com',
            first: 'Melba',
            last: 'Stiedemann',
            company: 'Rath Group',
            created_at: '2014-09-26T10:55:49.642Z',
            country: 'Andorra',
            type: 'Loyal'
          },
          {
            id: 25,
            email: 'gay_quigley98@gmail.com',
            first: 'Bonita',
            last: 'Hickle',
            company: 'Ledner, Jacobs and Schuster',
            created_at: '2015-03-03T13:32:26.071Z',
            country: 'Congo',
            type: 'Loyal'
          },
          {
            id: 26,
            email: 'mireille.conroy96@hotmail.com',
            first: 'Kali',
            last: 'Bailey',
            company: 'Bailey, McDermott and Kuphal',
            created_at: '2014-12-13T01:39:35.925Z',
            country: 'Tuvalu',
            type: 'Loyal'
          },
          {
            id: 27,
            email: 'candido.cormier89@gmail.com',
            first: 'Kristy',
            last: 'Quigley',
            company: 'Brown, Carter and Keeling',
            created_at: '2014-06-01T05:27:07.870Z',
            country: 'Burkina Faso',
            type: 'Loyal'
          },
          {
            id: 28,
            email: 'lola_altenwerth82@yahoo.com',
            first: 'Leanna',
            last: 'Dach',
            company: 'Fisher and Sons',
            created_at: '2014-09-19T09:39:20.201Z',
            country: 'Bahamas',
            type: 'Loyal'
          },
          {
            id: 29,
            email: 'willie36@hotmail.com',
            first: 'Hannah',
            last: 'OKeefe',
            company: 'Monahan Group',
            created_at: '2014-12-02T23:43:36.414Z',
            country: 'Guam',
            type: 'Loyal'
          },
          {
            id: 30,
            email: 'minerva10@gmail.com',
            first: 'Melyna',
            last: 'Carroll',
            company: 'Wolf Group',
            created_at: '2014-05-29T13:39:21.805Z',
            country: 'Indonesia',
            type: 'Loyal'
          }
        ]
      ));
      this.persons = JSON.parse(localStorage.getItem('customers'));
    }
  }

  getCurrentDate() {
    const current = new Date();
    return current.toISOString();
  }

  generateId() {
    const tmp = this.persons;
    tmp.sort((a, b) => b.id - a.id);
    return tmp[0].id++;
  }

  updateTableInstance() {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      dtInstance.init();
      this.dtTrigger.next();
    });
  }

  exportPDF() {
    const modalRef = this.modalService.open(SelectColumnsPopupComponent);
    modalRef.result.then((result) => {
      if (result && result.length) {
        const doc = new jsPDF();
        const cols = this.getHeaders(result);
        const rows = [];
        this.persons.forEach(element => {
          const tmp = this.getRowsByHeaders(result, element);
          rows.push(tmp);
        });
        doc.autoTable(cols, rows, { startY: 10 });
        doc.save('customer-report.pdf');
      }
    }, (reason) => {

    });
    modalRef.componentInstance.columns = this.columnsMapping;
  }

  addCustomer() {
    const modalRef = this.modalService.open(AddOrEditComponentComponent);
    modalRef.componentInstance.title = 'Add Customer Dialog';
    modalRef.result.then((result) => {
      if (result) {
        this.persons.push({
          id: this.generateId(),
          email: result.email,
          first: result.first,
          last: result.last,
          company: result.company,
          created_at: this.getCurrentDate(),
          country: result.country,
          type: 'New'
        });
        localStorage.setItem('customers', JSON.stringify(this.persons));
        this.updateTableInstance();
      }
    }, (reason) => {

    });
  }

  editCustomer(customer) {
    const modalRef = this.modalService.open(AddOrEditComponentComponent);
    modalRef.componentInstance.title = 'Edit Customer Dialog';
    modalRef.componentInstance.data = customer;
    modalRef.result.then((result) => {
      if (result) {
        this.persons.map((person) => {
          if (person.id === customer.id) {
            person.email = result.email;
            person.first = result.first;
            person.last = result.last;
            person.company = result.company;
            person.type = result.type;
          }
        });
        localStorage.setItem('customers', JSON.stringify(this.persons));
        this.updateTableInstance();
      }
    }, (reason) => {

    });
  }

  delCustomer(id) {
    this.modalService.open(ConfirmComponent).result.then((result) => {
      if (result === '1') {
        this.persons = this.persons.filter((person) => {
          return person.id !== id;
        });
        localStorage.setItem('customers', JSON.stringify(this.persons));
        this.updateTableInstance();
      }
    }, (reason) => {

    });
  }
}

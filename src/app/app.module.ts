import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { DataTablesModule } from 'angular-datatables';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AddOrEditComponentComponent } from './popup/add-or-edit-popup/add-or-edit-component.component';
import { ConfirmComponent } from './popup/confirm-popup/confirm.component';
import { FormsModule } from '@angular/forms';
import { SelectColumnsPopupComponent } from './popup/select-columns-popup/select-columns-popup.component';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { ChartsModule } from 'ng2-charts';
import { UnderscoreStatic } from 'underscore';

declare global {
  let _: UnderscoreStatic;
}

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    AddOrEditComponentComponent,
    ConfirmComponent,
    SelectColumnsPopupComponent,
    PieChartComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    DataTablesModule,
    NgbModule,
    ChartsModule
  ],
  entryComponents: [
    AddOrEditComponentComponent,
    ConfirmComponent,
    SelectColumnsPopupComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
